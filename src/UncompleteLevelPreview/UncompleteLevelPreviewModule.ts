import { NgModule } from '@angular/core';
import { UncompleteLevelPreviewComponent } from './UncompleteLevelPreviewComponent';

@NgModule({
    declarations: [ UncompleteLevelPreviewComponent ],
    entryComponents: [ UncompleteLevelPreviewComponent ],
    exports: [ UncompleteLevelPreviewComponent ]
})
export class UncompleteLevelPreviewModule {
}
