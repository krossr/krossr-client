import { Component } from '@angular/core';

@Component({
  selector: 'krossr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'krossr-client';
}
